FROM ubuntu:18.04

ARG UNAME=ubuntu
ARG GNAME=ubuntu
ARG UID=1000
ARG GID=1000


RUN groupadd --gid ${GID} ${GNAME} && \
    useradd --create-home --uid ${UID} --gid ${GID} --shell /bin/bash ${UNAME}

RUN apt-get update && apt-get install -y \
    git python3 python3-pip curl jq libz-dev libmysqlclient-dev \
    ca-certificates curl apt-transport-https lsb-release gnupg \
    build-essential libssl-dev

ADD build/coriolis-cd                 /usr/local/src/coriolis-cd
ADD build/python-coriolisclient       /usr/local/src/python-coriolisclient
ADD build/coriolis                    /usr/local/src/coriolis
ADD build/coriolis-provider-azure     /usr/local/src/coriolis-provider-azure
ADD build/coriolis-provider-oracle-vm /usr/local/src/coriolis-provider-oracle-vm
ADD build/coriolis-provider-oci       /usr/local/src/coriolis-provider-oci

RUN pip3 install python-novaclient python-cinderclient python-neutronclient awscli
RUN pip3 install -e /usr/local/src/coriolis-cd
RUN pip3 install -e /usr/local/src/python-coriolisclient
RUN pip3 install -e /usr/local/src/coriolis
RUN pip3 install -e /usr/local/src/coriolis-provider-azure
RUN pip3 install -e /usr/local/src/coriolis-provider-oracle-vm
RUN pip3 install -e /usr/local/src/coriolis-provider-oci

RUN curl -sL https://packages.microsoft.com/keys/microsoft.asc | \
    gpg --dearmor | \
    tee /etc/apt/trusted.gpg.d/microsoft.asc.gpg > /dev/null

RUN echo "deb [arch=amd64] https://packages.microsoft.com/repos/azure-cli/ $(lsb_release -cs) main" | \
    tee /etc/apt/sources.list.d/azure-cli.list

RUN apt-get update && apt-get install -y azure-cli
