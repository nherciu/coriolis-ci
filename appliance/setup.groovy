@Library('common-lib') _

currentBuild.displayName = "#${currentBuild.number}-${params.VM_IDENTIFIER}"

CI = new coriolis.ci.Appliance()

BASE_APPLIANCE_PROVIDERS = CI.get_appliance_providers()
if (params.BASE_APPLIANCE_TEMPLATE_NAME) {
    BASE_APPLIANCE_PROVIDERS[params.PLATFORM].env_info.template_name = params.BASE_APPLIANCE_TEMPLATE_NAME
}

APPLIANCE_CONN_INFO = BASE_APPLIANCE_PROVIDERS[params.PLATFORM].connection_info
APPLIANCE_ENV_INFO = BASE_APPLIANCE_PROVIDERS[params.PLATFORM].env_info


try {

    def appliance_ip = CI.deploy_base_appliance(
        params.PLATFORM,
        APPLIANCE_CONN_INFO,
        APPLIANCE_ENV_INFO,
        params.VM_IDENTIFIER)

    CI.build_appliance(
        appliance_ip,
        params.EXPORT_PROVIDERS,
        params.IMPORT_PROVIDERS,
        params.CUSTOM_REPO_OWNER_NAMES,
        params.CUSTOM_REPO_BRANCHES)

    CI.deploy_appliance(appliance_ip)

    def appliance_ssh_info = CI.get_appliance_default_ssh_info(appliance_ip)
    CI.new_appliance_password(appliance_ssh_info)

} catch (Exception e) {
    if (params.CLEANUP_FAILED_APPLIANCE == true) {
        CI.cleanup_appliance(
            params.PLATFORM,
            APPLIANCE_CONN_INFO,
            APPLIANCE_ENV_INFO,
            params.VM_IDENTIFIER)
    } else {
        println("Job parameter CLEANUP_FAILED_APPLIANCE is false.")
        println("The appliance deletion stage will be skipped.")
    }
    throw e
}
