#!/usr/bin/env bash

maxWait=${TIMEOUT:-600}

function get-value-from-json() {
    value="$1"
    jsonString="$2"
    grep "$value" <<< "$jsonString" | sed 's@[", +]@@g'| cut -d: -f2
}

# TODO: other VPC related resources/dependencies if required
# aws ec2 describe-network-acls --filters 'Name=vpc-id,Values='$vpc | grep NetworkAclId
# aws ec2 describe-vpc-peering-connections --filters 'Name=requester-vpc-info.vpc-id,Values='$vpc | grep VpcPeeringConnectionId
# aws ec2 describe-vpc-endpoints --filters 'Name=vpc-id,Values='$vpc | grep VpcEndpointId
# aws ec2 describe-nat-gateways --filter 'Name=vpc-id,Values='$vpc | grep NatGatewayId
# aws ec2 describe-vpn-connections --filters 'Name=vpc-id,Values='$vpc | grep VpnConnectionId
# aws ec2 describe-vpn-gateways --filters 'Name=attachment.vpc-id,Values='$vpc | grep VpnGatewayId

function aws-tag-name() {
    resourceId="$1"
    name="$2"
    aws ec2 create-tags --resources "$resourceId" --tags Key=Name,Value="$name"
}

function aws-vpc-get() {
    vpcId="$1"
    aws ec2 describe-vpcs --vpc-ids "$vpcId"
}

# VPC Getters return ids of the resource dependent on the vpc
function aws-vpc-get-gateways() {
    vpc="$1"
    aws ec2 describe-internet-gateways \
        --filters Name=attachment.vpc-id,Values="$vpc" \
        --query InternetGateways[].InternetGatewayId \
        --output text
}

function aws-vpc-get-subnets() {
    vpc="$1"
    aws ec2 describe-subnets \
        --filters Name=vpc-id,Values="$vpc" \
        --query Subnets[].SubnetId \
        --output text
}

function aws-vpc-get-route-tables() {
    vpc="$1"
    aws ec2 describe-route-tables \
        --filters Name=vpc-id,Values="$vpc" \
        --query RouteTables[].RouteTableId \
        --output text
}

function aws-vpc-get-security-groups() {
    vpc="$1"
    aws ec2 describe-security-groups \
        --filters Name=vpc-id,Values="$vpc" \
        --query SecurityGroups[].GroupId \
        --output text

}

function aws-vpc-get-instances() {
    vpc="$1"
    aws ec2 describe-instances \
        --filters Name=vpc-id,Values="$vpc" \
        --query Reservations[].Instances[].InstanceId \
        --output text
}

function aws-vpc-get-nics() {
    vpc="$1"
    aws ec2 describe-network-interfaces \
        --filters Name=vpc-id,Values="$vpc" \
        --query NetworkInterfaces[].NetworkInterfaceId \
        --output text
}

# Resource Deleters
function aws-vpc-gateway-detach() {
    vpc="$1"
    igw="$2"
    #TODO: check attachment
    aws ec2 detach-internet-gateway \
        --vpc-id "$vpc" \
        --internet-gateway-id "$igw"
}

function aws-gateway-delete() {
    igw="$1"
    if aws ec2 describe-internet-gateways --internet-gateway-ids "$igw" > /dev/null 2>&1; then
        aws ec2 delete-internet-gateway --internet-gateway-id "$igw"
    fi
}

function aws-subnet-delete() {
    subnetId="$1"
    if aws ec2 describe-subnets --subnet-ids "$subnetId" > /dev/null 2>&1; then
        aws ec2 delete-subnet --subnet-id "$subnetId"
    fi
}

function aws-route-table-delete() {
    routeTableId="$1"
    # route table exists and is not main (only gets deleted with vpc)
    if aws ec2 describe-route-tables --route-table-ids "$routeTableId" > /dev/null 2>&1 && \
       ! aws ec2 describe-route-tables --route-table-ids "$routeTableId" \
       --query RouteTables[].Associations[].Main --output text | grep -c "True" > /dev/null 2>&1; then
        aws ec2 delete-route-table --route-table-id "$routeTableId"
    fi
}

function aws-security-group-delete() {
    securityGroupId="$1"
    if aws ec2 describe-security-groups --group-ids "$securityGroupId" > /dev/null 2>&1 && \
       ! aws ec2 describe-security-groups --group-ids "$securityGroupId" \
       --query "SecurityGroups[].GroupName" --output text | grep -c "default" > /dev/null 2>&1; then
        aws ec2 delete-security-group --group-id "$securityGroupId"
    fi
}

function aws-nic-delete() {
    nicId="$1"
    if aws ec2 describe-network-interfaces --network-interface-ids "$nicId" > /dev/null 2>&1; then
        aws ec2 delete-network-interface --network-interface-id "$nicId"
    fi
}

function aws-volume-delete() {
    volumeId="$1"
    if aws ec2 describe-volumes --volume-ids "$volumeId" > /dev/null 2>&1; then
        aws ec2 delete-volume --volume-id "$volumeId"
    fi
}

function aws-vpc-delete() {
    vpcId="$1"
    if aws ec2 describe-vpcs --vpc-ids "$vpcId" > /dev/null 2>&1; then
        aws ec2 delete-vpc --vpc-id "$vpcId"
    fi
}

# VM Getters
function aws-vm-get-nics() {
    instanceId=$1
    aws ec2 describe-instances \
        --instance-ids "$instanceId" \
        --query "Reservations[].Instances[].NetworkInterfaces[].NetworkInterfaceId" \
        --output text
}

function aws-vm-get-volumes() {
    instanceId=$1
    aws ec2 describe-instances \
        --instance-ids "$instanceId" \
        --query "Reservations[].Instances[].BlockDeviceMappings[].Ebs.VolumeId" \
        --output text
}

function aws-vm-get-state() {
    # pending -> running -> shutting-down -> terminated
    instanceId=$1
    aws ec2 describe-instances \
        --instance-ids "$instanceId" \
        --query "Reservations[].Instances[].State.Name" \
        --output text
}

function aws-vm-terminate() {
    instanceId=$1
    aws ec2 terminate-instances --instance-ids "$instanceId"
}

function aws-ami-get-bytype() {
    osType="$1"
    osVersion="$2"
    if echo "$osType" | grep -iq "ubuntu"; then
        imageOwner=099720109477
        imageName="ubuntu/images/*$osVersion*"
    elif echo "$osType" | grep -iq "RHEL"; then
        imageOwner=309956199498
        imageName="RHEL-$osVersion*"
    else
        imageOwner="aws-marketplace"
        imageName="$osType*$osVersion*"
    fi
    aws ec2 describe-images --output text --owners "$imageOwner" \
        --filters \
            Name=name,Values="$imageName" \
            Name=architecture,Values=x86_64 \
            Name=state,Values=available \
            Name=virtualization-type,Values=hvm \
            Name=block-device-mapping.volume-type,Values=gp2 \
            Name=block-device-mapping.delete-on-termination,Values=true \
        --query "reverse(sort_by(Images, &CreationDate))[].ImageId | [0]"
}

# Setup
function aws-deploy-from-ami() {
    amiId="$1"
    vmName="$2"
    keyPair="$3"
    subnetId="$4"
    secGroupId="$5"

    cmd=""
    cmd+="aws ec2 run-instances"
    cmd+=" --tag-specifications ResourceType=instance,Tags=[{Key=Name,Value=$vmName}]"
    cmd+=" --image-id $amiId"
    cmd+=" --key-name $keyPair"
    cmd+=" --subnet-id $subnetId"
    cmd+=" --output json"
    instanceInfo=$($cmd)
    instanceId=$(get-value-from-json InstanceId "$instanceInfo")

    aws ec2 modify-instance-attribute --instance-id "$instanceId" --groups "$secGroupId" > /dev/null

    echo "Waiting for VM to start running."
    waited=0
    while [ "$(aws-vm-get-state "$instanceId" | grep -c running)" -eq 0 ]; do
        if [ "$waited" -ge "$maxWait" ]; then
            echo "Error: VM did not enter running state in time!"
            exit 1
        fi
        ((waited=waited+10))
    done
}

function aws-deploy-simple-ubuntu-1804() {
    vmName="$1"
    keyPair="$2"
    subnetId="$3"
    secGroupId="$4"

    # get latest image
    latestAmi=$(aws-ami-get-bytype ubuntu 18.04)

    aws-deploy-from-ami "$latestAmi" "$vmName" "$keyPair" "$subnetId" "$secGroupId"
}

function aws-deploy-simple() {
    osType="$1"
    osVersion="$2"
    vmName="$3"
    keyPair="$4"
    subnetId="$5"
    secGroupId="$6"

    latestAmi=$(aws-ami-get-bytype "$osType" "$osVersion")
    aws-deploy-from-ami "$latestAmi" "$vmName" "$keyPair" "$subnetId" "$secGroupId"
}

function aws-vm-cleanup() {
    instanceId="$1"

    nics=$(aws-vm-get-nics "$instanceId")
    volumes=$(aws-vm-get-volumes "$instanceId")

    aws-vm-terminate "$instanceId" > /dev/null

    echo "Waiting for vm to be terminated."
    waited=0
    while [ "$(aws-vm-get-state "$instanceId" | grep -c terminated)" -eq 0 ]; do
        if [ "$waited" -ge "$maxWait" ]; then
            echo "Error: VM did not terminate in time!"
            exit 1
        fi
        ((waited=waited+10))
    done
    for nic in $nics; do
        aws-nic-delete "$nic"
    done
    for volume in $volumes; do
        aws-volume-delete "$volume"
    done
}

# Setup VPC with 1 subnet and public ip through internet-gateway
# returns vpc-id
function aws-vpc-create-public() {
    vpcName="$1"

    # create vpc
    vpcInfo=$(aws ec2 create-vpc --cidr-block 10.10.0.0/16 --output json)
    vpcId=$(get-value-from-json VpcId "$vpcInfo")
    echo "vpcId: $vpcId"
    aws-tag-name "$vpcId" "$vpcName"

    # create gateway
    igwInfo=$(aws ec2 create-internet-gateway --output json)
    igwId=$(get-value-from-json InternetGatewayId "$igwInfo")

    # attach gateway
    aws ec2 attach-internet-gateway --internet-gateway-id "$igwId" --vpc-id "$vpcId"

    # create route in default route table
    rtbId=$(aws-vpc-get-route-tables "$vpcId")
    aws ec2 create-route --destination-cidr-block 0.0.0.0/0 \
        --route-table-id "$rtbId" --gateway-id "$igwId" > /dev/null

    # create subnet
    availabilityZone=$(aws ec2 describe-availability-zones \
        --query AvailabilityZones[0].ZoneName --output text)

    subnetInfo=$(aws ec2 create-subnet --cidr-block 10.10.0.0/24 --vpc-id "$vpcId"\
        --availability-zone "$availabilityZone" --output json)

    subnetId=$(get-value-from-json SubnetId "$subnetInfo")
    aws ec2 modify-subnet-attribute --map-public-ip-on-launch \
        --subnet-id "$subnetId"
    echo "SubnetId: $subnetId"

    # create security group
    sgInfo=$(aws ec2 create-security-group --group-name "$vpcId-sshrdp" \
        --description "rdp and ssh" --vpc-id "$vpcId" --output json)

    sgId=$(get-value-from-json GroupId "$sgInfo")


    aws ec2 authorize-security-group-ingress --group-id "$sgId" \
        --protocol tcp --port 22 --cidr 0.0.0.0/0
    aws ec2 authorize-security-group-ingress --group-id "$sgId" \
        --protocol tcp --port 3389 --cidr 0.0.0.0/0
    echo "SecurityGroupId: $sgId"
}

# Terminate All vms, get all the remaining resources on a VPC by ID and clean up
function aws-vpc-cleanup() {
    vpc="$1"

    vms=$(aws-vpc-get-instances "$vpc")
    echo "VM Instances $vms"
    for vm in $vms; do
        aws-vm-cleanup "$vm"
    done

    # delete remaining nics
    nics=$(aws-vpc-get-nics "$vpc")
    echo "Orphaned NICs $nics"
    for nic in $nics; do
        aws-nic-delete "$nic"
    done

    igws=$(aws-vpc-get-gateways "$vpc")
    echo "Internet Gateways $igws"
    for igw in $igws; do
        aws-vpc-gateway-detach "$vpc" "$igw"
        aws-gateway-delete "$igw"
    done

    subnets=$(aws-vpc-get-subnets "$vpc")
    echo "Subnets $subnets"
    for subnet in $subnets; do
        aws-subnet-delete "$subnet"
    done

    sgs=$(aws-vpc-get-security-groups "$vpc")
    echo "Security Groups $sgs"
    for sg in $sgs; do
        aws-security-group-delete "$sg"
    done

    rtbls=$(aws-vpc-get-route-tables "$vpc")
    echo "Route Tables $rtbls"
    for rtbl in $rtbls; do
        aws-route-table-delete "$rtbl"
    done

    aws-vpc-delete "$vpc"
}

function aws-configure() {
    accessKey="$1"
    secretAccessKey="$2"
    region="$3"

    aws configure set aws_access_key_id "$accessKey"
    aws configure set aws_secret_access_key "$secretAccessKey"
    aws configure set region "$region"
}